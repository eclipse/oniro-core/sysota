# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

$(eval $(call ZMK.Import,Silent))

DataInOut.Variables=Variables
define DataInOut.Template
$1.Variables ?= Project.Name Project.Version
$$(foreach v,$$($1.Variables),$$(eval $1.NameOf.$$v ?= $$v))
$1.NameOf.Project.Name = PROJECT_NAME
$1.NameOf.Project.Version = PROJECT_VERSION

$$(if $$(ZMK.IsOutOfTreeBuild),$$(eval $$(call ZMK.Expand,Directory,$$(CURDIR)/$$(dir $1))))
$$(if $$(ZMK.IsOutOfTreeBuild),$$(CURDIR)/$1) $1: $$(ZMK.OutOfTreeSourcePath)$1.in $$(if $$(value Configure.Configured),config.$$(Project.Name).mk) | $$(if $$(ZMK.IsOutOfTreeBuild),$$(CURDIR)/)$$(patsubst %/,%,$$(dir $1))
	$$(call Silent.Say,SED,$$@)
	$$(Silent.Command)$$(strip sed $$(foreach v,$$($1.Variables), -e 's!@$$($1.NameOf.$$v)@!$$($$v)!g') $$< >$$@)
$$(eval $$(call ZMK.Expand,AllClean,$1))
endef
