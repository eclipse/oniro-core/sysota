# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
Project.Name=sysota
Project.Version=1

# Set REUSE meta-data for the generated configuration files
Configure.LicenseIdentifier = Apache-2.0
Configure.CopyrightText = Huawei Inc.

include z.mk
$(if $(value ZMK.Version),,$(error The project depends on zmk -- Get it from https://github.com/zyga/zmk or ./get-zmk.sh;))

$(eval $(call ZMK.Import,Directories))
$(eval $(call ZMK.Import,Configure))

# Load hot-fix for issue related to symlinks.
include $(ZMK.SrcDir)/.zmk-hotfixes/zmk-hotfix-80.mk

# Re-define ZMK.Import with support for local modules.
define ZMK.Import
ifeq (,$1)
$$(error incorrect call to ZMK.Import, expected module name)
endif
ifeq (,$$(filter $1,$$(ZMK.ImportedModules)))
$$(if $$(findstring import,$$(DEBUG)),$$(info DEBUG: importing »$1«))
ZMK.ImportedModules += $1
ifneq (,$$(wildcard $$(ZMK.Path)/zmk/$1.mk))
include $$(ZMK.Path)/zmk/$1.mk
else ifneq (,$$(wildcard $$(ZMK.SrcDir)/.zmk-modules/$1.mk))
include $$(ZMK.SrcDir)/.zmk-modules/$1.mk
else
$$(error cannot import module $1 - no such module)
endif
endif
endef

# Go support.
Go.ImportPath = gitlab.com/zygoon/sysota
$(eval $(call ZMK.Import,Go))

# Go static analysis and code style tools.
$(eval $(call ZMK.Import,Go.GoStaticCheck))
$(eval $(call ZMK.Import,Go.GoConst))
$(eval $(call ZMK.Import,Go.GoErrCheck))
$(eval $(call ZMK.Import,Go.GoWsl))
$(eval $(call ZMK.Import,Go.GoIneffAssign))

# Spell checker.
Cspell.Options = '**/*.go' '**/*.md' '**/*.yaml' '**/*.json' '**/*.txt' '**/*.log' '**/*.mk' Makefile '.tours/*.tour' 'man/*.in'
$(eval $(call ZMK.Import,Cspell))

# Spread system tests.
Spread.Suites = \
	boot/piboot/spread.suite \
	boot/grub/spread.suite \
	cmd/spread.suite \
	man/spread.suite \
	ota/statehandler/spread.suite \
	rauc/spread.suite
Spread.TaskFilter = qemu:$(if $(value T),$(T))
Spread.Options = -v -reuse -resend $(Spread.TaskFilter)
$(eval $(call ZMK.Import,Spread))

# Reuse support.
$(eval $(call ZMK.Import,Reuse))

# Source archive
$(Project.Name)-$(Project.Version).tar.gz.Files += \
	$(Go.Sources) \
	$(Reuse.Sources) \
	$(Spread.Sources) \
	tests/bin/MATCH \
	tests/bin/NOMATCH \
	tests/lib/rauc.sh \
	$(wildcard $(ZMK.SrcDir)/.zmk-hotfixes/*.mk) \
	$(wildcard $(ZMK.SrcDir)/.zmk-modules/*.mk) \
	$(wildcard $(ZMK.SrcDir)/doc/adr/*.md) \
	$(wildcard $(ZMK.SrcDir)/*.md) \
	.vscode/settings.json \
	Makefile \
	README.md \
	cspell.json \
	data/dbus-1/system-services/org.oniroproject.sysota1.service \
	data/dbus-1/system.d/org.oniroproject.sysota1.conf \
	data/systemd/system/sysotad.service.in \
	man/sysota.state.ini.5.in \
	man/sysotad.conf.5.in
$(eval $(call ZMK.Expand,Tarball.Src,$(Project.Name)-$(Project.Version).tar.gz))

# D-Bus configuration
data/dbus-1/system-services/org.oniroproject.sysota1.service.InstallDir = $(datadir)/dbus-1/system-services
$(eval $(call ZMK.Expand,InstallUninstall,data/dbus-1/system-services/org.oniroproject.sysota1.service))
data/dbus-1/system.d/org.oniroproject.sysota1.conf.InstallDir = $(datadir)/dbus-1/system.d
$(eval $(call ZMK.Expand,InstallUninstall,data/dbus-1/system.d/org.oniroproject.sysota1.conf))

# Systemd configuration
data/systemd/system/sysotad.service.Variables=libexecdir
$(eval $(call ZMK.Expand,DataInOut,data/systemd/system/sysotad.service))
$(eval $(call ZMK.Expand,SystemdUnit,data/systemd/system/sysotad.service))

# Program: sysota-mux
$(if $(ZMK.IsOutOfTreeBuild),$(eval $(call ZMK.Expand,Directory,$(CURDIR)/$(dir cmd/sysota-mux))))
cmd/sysota-mux/sysota-mux: $(Go.Sources) | $(if $(ZMK.IsOutOfTreeBuild),$(CURDIR)/)$(patsubst %/,%,$(dir cmd/sysota-mux))
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(Go.Cmd) build -o $(if $(ZMK.IsOutOfTreeBuild),$(CURDIR)/)$@ $(Go.ImportPath)/$(dir $@)
$(eval $(call ZMK.Expand,AllClean,cmd/sysota-mux/sysota-mux))
cmd/sysota-mux/sysota-mux.InstallDir = $(libexecdir)/sysota
cmd/sysota-mux/sysota-mux.InstallMode = 755
$(eval $(call ZMK.Expand,InstallUninstall,cmd/sysota-mux/sysota-mux))

# Program: tests/lib/rauc.sh
tests/lib/rauc.sh.InstallDir = noinst
tests/lib/rauc.sh.Interpreter = sh
$(eval $(call ZMK.Expand,Script,tests/lib/rauc.sh))

# Manual pages.
$(eval $(call ZMK.Expand,DataInOut,man/sysotad.conf.5))
$(eval $(call ZMK.Expand,ManPage,man/sysotad.conf.5))
$(eval $(call ZMK.Expand,DataInOut,man/sysota.state.ini.5))
$(eval $(call ZMK.Expand,ManPage,man/sysota.state.ini.5))
$(eval $(call ZMK.Expand,DataInOut,man/sysota.boot-protocol.7))
$(eval $(call ZMK.Expand,ManPage,man/sysota.boot-protocol.7))
$(eval $(call ZMK.Expand,DataInOut,man/sysota.grub.7))
$(eval $(call ZMK.Expand,ManPage,man/sysota.grub.7))

# Symlink: sysotad -> sysota-mux
cmd/sysota-mux/sysotad.InstallDir = $(cmd/sysota-mux/sysota-mux.InstallDir)
cmd/sysota-mux/sysotad.SymlinkTarget = sysota-mux
$(eval $(call ZMK.Expand,Symlink,cmd/sysota-mux/sysotad))

# Symlink: sysotactl -> sysota-mux
cmd/sysota-mux/sysotactl.InstallDir = $(bindir)
cmd/sysota-mux/sysotactl.SymlinkTarget = $(cmd/sysota-mux/sysota-mux.InstallDir)/sysota-mux
$(eval $(call ZMK.Expand,Symlink,cmd/sysota-mux/sysotactl))

# Symlink: rauc-pre-install-handler -> sysota-mux
cmd/sysota-mux/rauc-pre-install-handler.InstallDir = $(cmd/sysota-mux/sysota-mux.InstallDir)
cmd/sysota-mux/rauc-pre-install-handler.SymlinkTarget = sysota-mux
$(eval $(call ZMK.Expand,Symlink,cmd/sysota-mux/rauc-pre-install-handler))

# Symlink: rauc-post-install-handler -> sysota-mux
cmd/sysota-mux/rauc-post-install-handler.InstallDir = $(cmd/sysota-mux/sysota-mux.InstallDir)
cmd/sysota-mux/rauc-post-install-handler.SymlinkTarget = sysota-mux
$(eval $(call ZMK.Expand,Symlink,cmd/sysota-mux/rauc-post-install-handler))

# Symlink: rauc-custom-boot-handler -> sysota-mux
cmd/sysota-mux/rauc-custom-boot-handler.InstallDir = $(cmd/sysota-mux/sysota-mux.InstallDir)
cmd/sysota-mux/rauc-custom-boot-handler.SymlinkTarget = sysota-mux
$(eval $(call ZMK.Expand,Symlink,cmd/sysota-mux/rauc-custom-boot-handler))

# The entr-check% family of targets runs tests continuously, as the code is
# modified. The set of dependencies is the same as the set of files for the
# project tarball. This is an imperfect but practical simplification.
.PHONY: entr-check%
entr-check-go-test entr-check: entr-check%: $(sort $($(Project.Name)-$(Project.Version).tar.gz.Files))
	echo $^ | tr ' ' '\n' | entr -c $(MAKE) check$*
